let calc = new Vue({
    el: '#vue-table',
    data: {
        rows: [],
        rowsCount: 10,
        columnCount: 10
    },
    methods: {
        createTable: function () {
            this.rows = [];
            this.rowsCount = this.rowsCount > 10 ? 10 : this.rowsCount;
            this.columnCount = this.columnCount > 10 ? 10 : this.columnCount;

            for (let i = 0; i < this.rowsCount + 1; i++) {
                let newRow  = [];
                for (let k = 0; k < this.columnCount + 1; k++) {
                    let box = {};
                    if (i === 0 && k === 0){
                        box = {
                            id: i,
                            content: 'X',
                            class: 'multiply'
                        }
                    }else if(k === 0 || i === 0){
                        box = {
                            id: i,
                            content: i === 0 ? k : i,
                            class: 'border'
                        }
                    }else{
                        box = {
                            id: i,
                            content: k * i,
                            class: (k + i) % 2  ? 'odd' : 'even'
                        }
                    }
                    newRow.push(box);
                }
                this.rows.push(newRow);
            }
        }
    },
    created: function () {
        this.createTable();
    }
});


/**
 * Функция для распечатки части страницы...
 * @param strid
 * @constructor
 */
function сallPrint(strid) {
    window.print();
    return true;

}
